package vn.vtop.wapapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.vmodev.common.utility.FileUtil;

@SuppressLint("NewApi")
public class MainActivity extends Activity {

	private WebView webView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		webView = (WebView) findViewById(R.id.webview);
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				Log.i("go url", url);
				if (url.endsWith(".apk") || url.endsWith(".jar")
						|| url.endsWith(".jad") || url.endsWith(".ipa")) {
					Uri uri = Uri.parse(url);
					DownloadManager.Request r = new DownloadManager.Request(uri);

					// This put the download in the same Download dir the
					// browser uses
					r.setDestinationInExternalPublicDir(
							Environment.DIRECTORY_DOWNLOADS,
							FileUtil.getFullFileName(url));
					if (Build.VERSION.SDK_INT >= 11) {
						r.setNotificationVisibility(Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
					}
					// Start download
					DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
					dm.enqueue(r);
					return false;
				} else {
					view.loadUrl(url);
					return true;
				}

			}
		});
		webView.loadUrl("http://vtop.vn");
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			switch (keyCode) {
			case KeyEvent.KEYCODE_BACK:
				if (webView.canGoBack()) {
					webView.goBack();
				} else {
					finish();
				}
				return true;
			}

		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
